### "Features"

* Specify directory which will be browsed.
* Download files through the interface.
* Doesn't allow access to parent directories.
* Doesn't display or allow access to files/directories beginning with '.' (dot).
* Can password-protect access to the file browser.
* Syntax highlighting for various files. Using JavaScript SyntaxHighlighter: http://alexgorbatchev.com/SyntaxHighlighter/

### Not yet implemented, but remain in my mind

* Resume download support.
* Migrate to use my PHP MVC framework (publish it as well ;) and tidying up the code as well.
* Align output fields to columns without tables (why without tables..? idk)
* Pass W3C validator.

### General usage

The folder 'school' contains everything necessary to deploy this file browser. Change '$ROOTDIR' variable in 'school/index.php' to whichever directory you wish the file browser to browse.

Note: If you rename the directory 'school', change it in 'school/.htaccess' as well (Apache 1.3.. See note below).

### Password

Create a file 'school/password' and write your password in there. Empty or non-existing password file means no login is required.

### Other noteworthy stuff

* The directory access rules (.htaccess) were written to work in old Apache 1.3 as well.
* Password changed on the fly (in the file), while authenticated users have their cookies stored on the servers, forces the authenticated users to log in again.
* I've modified SyntaxHighlighter (removed 'overflow' property in '.syntaxhighlighter textarea {' and '.syntaxhighlighter {' in file 'shCore.css') to make the scrollbars disappear.