<?php
/* Copyright 2011 Gima. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Gima ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Gima OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Gima. */

// --
$ROOTDIR = 'root';
// --

// TODO: resume support
// TODO: trim file names to align and if too long, to truncate with "..."
 
define('ROOTDIR', str_replace('\\', '/', realpath($ROOTDIR)));
unset($ROOTDIR);

if (false) {
	error_reporting(E_ALL | E_NOTICE);
}
else {
	error_reporting(0);
}

if (is_file('password')) {
	$pw = implode('', file('password'));
	if (strlen($pw) == 0) $pw = null;
} else {
	$pw = null;
}

define('PASSWORD', $pw);

if (PASSWORD !== null) {
	session_set_cookie_params(0, $_SERVER["REQUEST_URI"]);
	session_start();
	if (is_file('easteregg.php')) include_once('easteregg.php'); // my easter egg for special password
	if (@$_POST['pw'] === PASSWORD) {
		$_SESSION['authorized'] = PASSWORD;
		header('Location: .');
		die();
	}
	else if (isset($_GET['logout'])) {
		$_SESSION['authorized'] = null;
		header('Location: .');
		die();
	}
	
	if (@$_SESSION['authorized'] !== PASSWORD) {
		?><body onload="document.forms[0].pw.focus();"><form action="" method="post"><input name="pw" type="password" autocomplete="off"></input></form><?php
		die();
	}
}

$requestPath = urldecode(@$_GET['path']);
if (strlen($requestPath) == 0) $requestPath = '/';
$translatedPath = str_replace('\\', '/', realpath(ROOTDIR."/$requestPath"));

if ($requestPath[0] !== '/' || strpos($requestPath, '/.') !== false || strpos($requestPath, '//') !== false || strpos($translatedPath, ROOTDIR) !== 0) {
	error('Request not allowed');
}

if (is_dir($translatedPath)) {
	printDirectoryListing($translatedPath);
}
else if (is_file($translatedPath)) {
	if (isset($_GET['view'])) {
		$viewFile = $translatedPath;
		$viewFileDisplayName = substr($translatedPath, strlen(ROOTDIR));
		include_once('view.php');
		die;
	}
	else {
		sendFile($translatedPath);
	}
}
else {
	error('Cannot handle the specified file.');
}

// -- functions

function printDirectoryListing($path) {
	echo '<pre>';
	if (PASSWORD !== null) {
		echo "<a href='?logout'>Logout</a>\n\n";
	}
	list($dirs, $files) = listDirEntries($path);
	
	$upperDir = dirname($path);
	if ($path !== ROOTDIR) {
		$upperDir = urlencode(substr($upperDir, strlen(ROOTDIR)));
		echo "[DIR] <a href='?path=$upperDir'>..</a>\n";
	}
	else {
		echo "[DIR] ..<font color='grey'> // A-aa, topmost directory.</font>\n";
	}
	
	foreach ($dirs as $entry) {
		echo outputPrettyEntry($path, $entry, false)."\n";
	}
	
	foreach ($files as $entry) {
		echo outputPrettyEntry($path, $entry, true)."\n";
	}
	
	if ((count($dirs) === 0) && (count($files) === 0)) {
		echo '      This directory is empty. Or is it? ;)';
		return;
	}
}

function outputPrettyEntry($path, $entry, $isFile) {
	
	$linkURL = substr($path, strlen(ROOTDIR))."/$entry";
	
	/*if ($path === ROOTDIR) {
		$linkURL .= "/$entry";
	} else {
		$linkURL .= "/$entry";
	}*/
	
	$highlightLink = '';
	
	if ($isFile) {
		$prefixText = '     ';
		$sizeText = getPrettySize(filesize("$path/$entry"));
		$mimeText = getMimeType("$path/$entry");
		$linkText = $entry;
		$fileExt = pathinfo("$path/$entry"); $fileExt = @$fileExt['extension'];

		switch ($fileExt) {
		case 'php':
		case 'java':
		case 'php':
		case 'java':
		case 'xml':
		case 'htm':
		case 'html':
		case 'js':
		case 'cpp':
		case 'c':
		case 'sh':
		case 'css':
		case 'diff':
			$highlightLink = ' [<a href="?path='.urlencode($linkURL).'&amp;view">ツ</a>] ';
		}
	}
	else {
		$prefixText = '[DIR]';
		$sizeText = '';
		$mimeText = '';
		$linkText = "$entry/";
		$linkURL .= '/';
	}
	
	echo sprintf(
		"%s <a href='?path=%s'>%s</a>%s&nbsp;&nbsp;%s&nbsp;&nbsp;%s",
		$prefixText,
		urlencode($linkURL),
		htmlentities($linkText),
		$highlightLink,
		$sizeText,
		$mimeText
	);
}

function outputLink($path) {
	$path = substr($path, strlen(ROOTDIR));
	return $path;
}

function getPrettySize($size) {
	if ($size >= 1073741824) { $divisor = 1073741824; $postfix= 'GiB'; }
	else if ($size >= 1048576) { $divisor = 1048576; $postfix= 'MiB'; }
	else if ($size >= 1024) { $divisor = 1024; $postfix= 'KiB'; }
	else return $size.'B';
	
	return round($size/$divisor) . $postfix;
}

function listDirEntries($path) {
	if (substr($path, -1, 1) !== '/') $path .= '/';
	$h = opendir($path);
	if ($h === false) error('Could not get directory listing', 500);
	
	$dirs = array();
	$files = array();
	
	while (($entry = readdir($h)) != false) {
		if ($entry[0] == '.') continue;
		if (is_dir($path.$entry)) $dirs[] = $entry;
		else if (is_file($path.$entry)) $files[] = $entry;
	}
	
	@closedir($h);
	return array($dirs, $files);
}

function sendFile($path) {
	$mimeType = getMimeType($path);
	$fileName = basename($path);
	$fileSize = filesize($path);
	
	if (@$fileName[0] === '.') error('Not authorized to read file', 401);
	if ($fileSize === false) error('Error while getting file size', 500);
	
	header("Content-Type: $mimeType");
	header("Content-Disposition: inline; filename=\"$fileName\"");
	header("Content-Length: $fileSize");

	$h = @fopen($path,'r');
	if ($h === false) error('Error while reading file', 500);
	fpassthru($h);
	@fclose($h);
}

function getMimeType($path) {
	$h = finfo_open(FILEINFO_MIME_TYPE);
	if ($h === false) error('Error while getting file MIME type', 500);
	
	$result = finfo_file($h, $path);
	if ($result === false) error('Error while getting file MIME type', 500);
	
	@finfo_close($h);
	return $result;
}

// -- functions, misc

function error($error_msg='', $header_code=404, $header_msg=null) {
	// list from http://framework.zend.com/svn/framework/laboratory/Zend_Service_SecondLife/library/Zend/Http/Response.php
	$header_messages = array(
        // Informational 1xx
        100 => 'Continue',
        101 => 'Switching Protocols',

        // Success 2xx
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',

        // Redirection 3xx
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',  // 1.1
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        // 306 is deprecated but reserved
        307 => 'Temporary Redirect',

        // Client Error 4xx
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',

        // Server Error 5xx
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        509 => 'Bandwidth Limit Exceeded'
    );
	
	$http_proto = $_SERVER['SERVER_PROTOCOL'];
	if ($http_proto === null) $http_proto = 'HTTP/1.1';
	
	if ($header_msg === null) {
		$header_msg = @$header_messages[$header_code];
		if ($header_msg === null) $header_msg = 'Unknown message';
	}
	
	header("{$http_proto} {$header_code} {$header_msg}");
	die($error_msg);
}
?>
