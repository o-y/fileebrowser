<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en_US">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<link href="syntaxhighlighter/css/shCore.css" rel="stylesheet" type="text/css" />
	<link href="syntaxhighlighter/css/shThemeEclipse.css" rel="stylesheet" type="text/css" />
	<title></title>
	<style type="text/css">
		html,body { margin:0; padding:0; width:100%; height:100%; }
		h3 { margin:0.5em; padding:0.2em; background-color:#DFDFDF; }
	</style>
</head>
<body>

<?php
$brush = strtolower(preg_replace('/.*\.([^\.]*)$/', '\\1', $viewFile));

switch ($brush) {
	case 'php':
		$brushFile = 'Php';
		break;
	case 'java':
		$brushFile = 'Java';
		break;
	case 'htm':
		$brush = 'html';
	case 'xml':
	case 'html':
		$brushFile = 'Xml';
		break;
	case 'js':
		$brushFile = 'JScript';
		break;
	case 'cpp':
	case 'c':
		$brushFile = 'Cpp';
		break;
	case 'sh':
		$brushFile = 'Bash';
		break;
	case 'css':
		$brushFile = 'Css';
		break;
	case 'diff':
		$brushFile = 'Diff';
		break;
	default:
		//error('No highlighter found for this file extension.');
		$brush = 'plain';
		$brushFile = 'Plain';
		break;
}
?>
<h3><?php echo htmlentities($viewFileDisplayName, ENT_QUOTES, 'UTF-8'); ?></h3>

<script type="text/javascript" src="syntaxhighlighter/js/shCore.js"></script>
<script type="text/javascript" src="syntaxhighlighter/js/shBrush<?php echo $brushFile ?>.js"></script>

<pre class="brush:<?php echo $brush; ?>; auto-links:false;">
<?php
$data = implode('',file($viewFile));
$data = str_replace('<', '&lt;', $data);
$data = str_replace('>', '&gt;', $data);
echo $data;
?>
</pre>

<script type="text/javascript">
	SyntaxHighlighter.all()
</script>

</body>
</html>